// Import thư viện express.js vào. Dạng Import express from "express";
const express = require("express");
const mongoose = require("mongoose");
const path = require("path");
const {router} = require("./app/routes/router");
const {userRouter} = require("./app/routes/userRouter");
const {DiceHistoryRouter} = require("./app/routes/diceHistoryRouter");
const {PrizeRouter} = require("./app/routes/prizeRouter");
const {VoucherRouter} = require("./app/routes/voucherRouter");
const {prizeHistoryRouter} = require("./app/routes/prizeHistoryRouter");
const {voucherHistoryRouter} = require("./app/routes/voucherHistoryRouter");
const {diceRouter} = require("./app/routes/diceRouter");


// Khởi tạo app express
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
const app = new express();
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))  

app.use(express.static(__dirname+ "/views"))



// Khai báo sẵn 1 port trên hệ thống
const port = 8000;

mongoose.connect("mongodb://localhost:27017/CRUD_luckyDice", (err) => {
    if(err) {
        throw err;
    }

    console.log("Connect MongoDB successfully!");
})


app.use("/",router);
app.use("/",userRouter);
app.use("/",DiceHistoryRouter);
app.use("/",PrizeRouter);
app.use("/",VoucherRouter);
app.use("/",prizeHistoryRouter);
app.use("/",voucherHistoryRouter);
app.use("/",diceRouter);

// Khai báo API
app.get("/",(req,res)=> {
    res.sendFile(path.join(__dirname+ "/views/luckyDice.html"))
})

 

app.listen(port, () => {
    console.log("App listening on port: ", port);
});
