const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const DiceHistory = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: "users",
        required: true,
    },
    dice: {
        type: Number, 
        default: () => Math.floor(Math.random() * 6) + 1,
        required: true,
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("diceHistory",DiceHistory)