const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const PrizeHistory = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: "users",
        required: true,
    },
    prize: {
        type: mongoose.Types.ObjectId,
        ref: "prizes",
        required: true,
    },
}, {
    timestamps: true
})

module.exports = mongoose.model("PrizeHistory",PrizeHistory)