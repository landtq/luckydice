const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const User = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("users",User)