const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const VoucherHistory = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        ref: "users",
        required: true,
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "vouchers",
        required: true,
    },
}, {
    timestamps: true
})

module.exports = mongoose.model("VoucherHistory",VoucherHistory)