const mongoose = require("mongoose");

const voucherModel = require("../models/voucherModel");

//create User
const createVoucher = (req, res) => {
    let body = req.body;
    if (!body.code) {
        return res.status(400).json({
            message: `code is required`
        })
    }
    if (!Number.isInteger(body.discount)) {
        return res.status(400).json({
            message: `discount is required`
        })
    }
    let newVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note,
    }
    voucherModel.create(newVoucher, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create successful`,
            newVoucher: data
        })
    })
}

//get all User
const getAllVoucher = (req, res) => {
    voucherModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `get all Voucher list success`,
            voucherList: data
        })
    })
}


//get Prize by Id
const getVoucherById = (req, res) => {
    let voucherId = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        })
    }
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: ` get Voucher by Id success`,
            voucher: data
        })
    })

}

//update User by Id
const updateVoucherById = (req, res) => {
    let voucherId = req.params.voucherId;
    let body = req.body;;
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        })
    }
    if (body.code !== undefined && body.code == "") {
        return res.status(400).json({
            message: `code is required`
        })
    }
    if(body.discount !== undefined &&(!Number.isInteger(body.discount) || body.discount < 0)){
        return res.status(400).json({
          message: 'Phần trăm giảm giá không hợp lệ'
        }) 
      }
    let updateVoucher = {
        code: body.code,
        discount: body.discount,
        note: body.note
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updateVoucher: data
        })
    })
}

//delete by User Id
const deleteByVoucherId = (req, res) => {
    let voucherId = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        })
    }
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: `delete success`
        })
    })
}


module.exports = {
    createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteByVoucherId
}


