const mongoose =  require("mongoose");

const UserModel = require("../models/userModel");

//create User
const createUser = (req, res) => {
    let body = req.body;
    if (!body.username) {
        return res.status(400).json({
            message: `username is required`
        })
    }
    if (!body.firstname) {
        return res.status(400).json({
            message: `firstname is required`
        })
    }
    if (!body.lastname) {
        return res.status(400).json({
            message: `lastname is required`
        })
    }
    let newUser = {
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
    }
    UserModel.create(newUser, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create successful`,
            newUser: data
        })
    })
}

//get all User
const getAllUser = (req, res) => {
    UserModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `get all User list success`,
            UserList: data
        })
    })
}


//get User by Id
const getUserById = (req, res) => {
    let UserId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(UserId)) {
        return res.status(400).json({
            message: `UserId is invalid`
        })
    }
    UserModel.findById(UserId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: ` get User by Id success`,
            User: data
        })
    })

}

//update User by Id
const updateUserById = (req, res) => {
    let UserId = req.params.userId;
    let body = req.body;;
    if (!mongoose.Types.ObjectId.isValid(UserId)) {
        return res.status(400).json({
            message: `UserId is invalid`
        })
    }
    if (body.username !== undefined && body.username == "") {
        return res.status(400).json({
            message: `username is required`
        })
    }
    if (body.firstname !== undefined && body.firstname == "") {
        return res.status(400).json({
            message: `firstname is required`
        })
    }
    if (body.lastname !== undefined && body.lastname == "") {
        return res.status(400).json({
            message: `lastname is required`
        })
    }
    let updateUser = {
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname
    }
    UserModel.findByIdAndUpdate(UserId, updateUser, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updateCourse: data
        })
    })
}

//delete by User Id
const deleteByUserId = (req,res) => {
    let UserId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(UserId)) {
        return res.status(400).json({
            message: `UserId is invalid`
        })
    }
    UserModel.findByIdAndDelete(UserId, (error,data)=> {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: `delete success`
        })
    })
}


module.exports = {
    createUser, getAllUser, getUserById, updateUserById,deleteByUserId
}


