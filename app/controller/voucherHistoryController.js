const mongoose = require("mongoose");

const voucherHistory = require("../models/voucherHistoryModel");

const createvoucherHistory = (req, res) => {
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `userId is invalid`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        })
    }
    let newvoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        voucher: body.voucher
    }
    voucherHistory.create(newvoucherHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create successful`,
            newvoucherHistory: data
        })
    })
}

//get all vouchervoucherHistory
const getAllvoucherHistory = (req, res) => {
    let {user} = req.query;
    let condition = {};
    if(user) {
        condition.user = user
    }
    voucherHistory.find(condition,(error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `get all voucherHistory list success`,
            voucherHistorylist: data
        })
    })
}


//get vouchervoucherHistory by Id
const getvoucherHistoryById = (req, res) => {
    let voucherHistoryId = req.params.voucherHistoryId;
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            message: `voucherHistoryId is invalid`
        })
    }

    voucherHistory.findById(voucherHistoryId)
        .populate("user")
        .populate("voucher")
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: error.message
                })
            }
            return res.status(200).json({
                message: ` get voucherHistory by Id success`,
                voucherHistory: data
            })
        })
}


//update voucherHistory by Id
const updatevoucherHistoryById = (req, res) => {
    let voucherHistoryId = req.params.voucherHistoryId;
    let body = req.body;;
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            message: `voucherHistoryId is invalid`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `userId is invalid`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return res.status(400).json({
            message: `voucherId is invalid`
        })
    }
    let updatevoucherHistory = {
        user: body.user,
        voucher: body.voucher
    }
    voucherHistory.findByIdAndUpdate(voucherHistoryId, updatevoucherHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updatevoucher: data
        })
    })
}

//delete by vouchervoucherHistory Id
const deleteByvoucherHistoryId = (req, res) => {
    let voucherHistoryId = req.params.voucherHistoryId;
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            message: `voucherHistoryId is invalid`
        })
    }
    voucherHistory.findByIdAndDelete(voucherHistoryId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: `delete success`
        })
    })
}


module.exports = {
    createvoucherHistory, getAllvoucherHistory, getvoucherHistoryById, updatevoucherHistoryById, deleteByvoucherHistoryId
}


