const mongoose =  require("mongoose");

const diceHistory = require("../models/diceHistoryModel");

const createDiceHistory = (req, res) => {
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `userId is invalid`
        })
    }

    let newDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: body.user
    }
    diceHistory.create(newDiceHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create successful`,
            newDiceHistory: data
        })
    })
}

//get all DicecreateDiceHistory
const getAllDiceHistory = (req, res) => {
    let {user} = req.query;
    let condition = {};
    if(user) {
        condition.user = user
    }
    diceHistory.find(condition,(error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `get all DiceHistory list success`,
            DiceHistorylist: data
        })
    })
}


//get DicecreateDiceHistory by Id
const getDiceHistoryById = (req, res) => {
    let DiceHistoryId = req.params.diceHistoryId;
    if (!mongoose.Types.ObjectId.isValid(DiceHistoryId)) {
        return res.status(400).json({
            message: `DiceHistoryId is invalid`
        })
    }
    diceHistory.findById(DiceHistoryId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: ` get DiceHistory by Id success`,
            DiceHistory: data
        })
    })

}

//update DiceHistory by Id
const updateDiceHistoryById = (req, res) => {
    let DiceHistoryId = req.params.diceHistoryId;
    let body = req.body;;
    if (!mongoose.Types.ObjectId.isValid(DiceHistoryId)) {
        return res.status(400).json({
            message: `DiceHistoryId is invalid`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `userId is invalid`
        })
    }
    let updateDiceHistory = {
        user: body.user,
        dice: body.dice
    }
    diceHistory.findByIdAndUpdate(DiceHistoryId, updateDiceHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updateCourse: data
        })
    })
}

//delete by DicecreateDiceHistory Id
const deleteByDiceHistoryId = (req,res) => {
    let DiceHistoryId = req.params.diceHistoryId;
    if (!mongoose.Types.ObjectId.isValid(DiceHistoryId)) {
        return res.status(400).json({
            message: `DiceHistoryId is invalid`
        })
    }
    diceHistory.findByIdAndDelete(DiceHistoryId, (error,data)=> {
        if(error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: `delete success`
        })
    })
}


module.exports = {
    createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById,deleteByDiceHistoryId
}


