const mongoose = require("mongoose");

const prizeHistory = require("../models/prizeHistoryModel");

const createprizeHistory = (req, res) => {
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `userId is invalid`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.prize)) {
        return res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    let newprizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        prize: body.prize
    }
    prizeHistory.create(newprizeHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create successful`,
            newprizeHistory: data
        })
    })
}

//get all prizeprizeHistory
const getAllprizeHistory = (req, res) => {
    let {user} = req.query;
    let condition = {};
    if(user) {
        condition.user = user
    }
    prizeHistory.find(condition,(error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `get all prizeHistory list success`,
            prizeHistorylist: data
        })
    })
}


//get prizeprizeHistory by Id
const getprizeHistoryById = (req, res) => {
    let prizeHistoryId = req.params.prizeHistoryId;
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            message: `prizeHistoryId is invalid`
        })
    }

    prizeHistory.findById(prizeHistoryId)
        .populate("user")
        .populate("prize")
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: error.message
                })
            }
            return res.status(200).json({
                message: ` get prizeHistory by Id success`,
                prizeHistory: data
            })
        })
}


//update prizeHistory by Id
const updateprizeHistoryById = (req, res) => {
    let prizeHistoryId = req.params.prizeHistoryId;
    let body = req.body;;
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            message: `prizeHistoryId is invalid`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            message: `userId is invalid`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.prize)) {
        return res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    let updateprizeHistory = {
        user: body.user,
        prize: body.prize
    }
    prizeHistory.findByIdAndUpdate(prizeHistoryId, updateprizeHistory, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updatePrize: data
        })
    })
}

//delete by prizeprizeHistory Id
const deleteByprizeHistoryId = (req, res) => {
    let prizeHistoryId = req.params.prizeHistoryId;
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            message: `prizeHistoryId is invalid`
        })
    }
    prizeHistory.findByIdAndDelete(prizeHistoryId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: `delete success`
        })
    })
}


module.exports = {
    createprizeHistory, getAllprizeHistory, getprizeHistoryById, updateprizeHistoryById, deleteByprizeHistoryId
}


