const mongoose = require("mongoose");

const prizeModel = require("../models/prizeModel");

//create User
const createPrize = (req, res) => {
    let body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: `name is required`
        })
    }
    let newPrize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    }
    prizeModel.create(newPrize, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create successful`,
            newPrize: data
        })
    })
}

//get all User
const getAllPrize = (req, res) => {
    prizeModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `get all Prize list success`,
            UserList: data
        })
    })
}


//get Prize by Id
const getPrizeById = (req, res) => {
    let prizeId = req.params.prizeId;
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    prizeModel.findById(prizeId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: ` get Prize by Id success`,
            User: data
        })
    })

}

//update User by Id
const updatePrizeById = (req, res) => {
    let prizeId = req.params.prizeId;
    let body = req.body;;
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    if (body.name !== undefined && body.name == "") {
        return res.status(400).json({
            message: `name is required`
        })
    }
    let updatePrize = {
        name: body.name,
        description: body.description
    }
    prizeModel.findByIdAndUpdate(prizeId, updatePrize, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updatePrize: data
        })
    })
}

//delete by User Id
const deleteByPrizeId = (req, res) => {
    let prizeId = req.params.prizeId;
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            message: `prizeId is invalid`
        })
    }
    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: `delete success`
        })
    })
}


module.exports = {
    createPrize, getAllPrize, getPrizeById, updatePrizeById, deleteByPrizeId
}


