const express = require("express");

const voucherHistoryRouter = express.Router();



const { createvoucherHistory, getAllvoucherHistory, getvoucherHistoryById, updatevoucherHistoryById, deleteByvoucherHistoryId } = require("../controller/voucherHistoryController");

voucherHistoryRouter.get("/voucher-histories", getAllvoucherHistory)

voucherHistoryRouter.get("/voucher-histories/:voucherHistoryId", getvoucherHistoryById)

voucherHistoryRouter.post("/voucher-histories", createvoucherHistory)

voucherHistoryRouter.put("/voucher-histories/:voucherHistoryId", updatevoucherHistoryById)

voucherHistoryRouter.delete("/voucher-histories/:voucherHistoryId", deleteByvoucherHistoryId)

module.exports = voucherHistoryRouter;




module.exports = { voucherHistoryRouter }