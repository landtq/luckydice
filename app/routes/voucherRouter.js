const express = require("express");

const VoucherRouter = express.Router();

const {createVoucher, getAllVoucher, getVoucherById, updateVoucherById,deleteByVoucherId} = require("../controller/voucherController");

VoucherRouter.get("/vouchers",getAllVoucher)

VoucherRouter.get("/vouchers/:voucherId",getVoucherById)

VoucherRouter.post("/vouchers", createVoucher)

VoucherRouter.put("/vouchers/:voucherId",updateVoucherById)

VoucherRouter.delete("/vouchers/:voucherId",deleteByVoucherId)

module.exports = {VoucherRouter};






