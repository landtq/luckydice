const express = require("express");
const PrizeRouter = express.Router();


const { createPrize, getAllPrize, getPrizeById, updatePrizeById, deleteByPrizeId} = require("../controller/prizeController");

PrizeRouter.get("/prizes",getAllPrize)

PrizeRouter.get("/prizes/:prizeId",getPrizeById)

PrizeRouter.post("/prizes", createPrize)

PrizeRouter.put("/prizes/:prizeId",updatePrizeById)

PrizeRouter.delete("/prizes/:prizeId",deleteByPrizeId)

module.exports =  {PrizeRouter};







module.exports = {PrizeRouter}