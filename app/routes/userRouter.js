const express = require("express");

const userRouter = express.Router();



const {createUser,getAllUser,getUserById,updateUserById,deleteByUserId} = require("../controller/userController");

userRouter.get("/users",getAllUser)

userRouter.get("/users/:userId",getUserById)

userRouter.post("/users", createUser)

userRouter.put("/users/:userId",updateUserById)

userRouter.delete("/users/:userId",deleteByUserId)

module.exports = userRouter;







module.exports = {userRouter}