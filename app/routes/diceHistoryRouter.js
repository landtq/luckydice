const express = require("express");
const DiceHistoryRouter = express.Router();


const {createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById,deleteByDiceHistoryId} = require("../controller/DiceHistoryController");

DiceHistoryRouter.get("/dice-histories",getAllDiceHistory)

DiceHistoryRouter.get("/dice-histories/:diceHistoryId",getDiceHistoryById)

DiceHistoryRouter.post("/dice-histories", createDiceHistory)

DiceHistoryRouter.put("/dice-histories/:diceHistoryId",updateDiceHistoryById)

DiceHistoryRouter.delete("/dice-histories/:diceHistoryId",deleteByDiceHistoryId)

module.exports = DiceHistoryRouter;







module.exports = {DiceHistoryRouter}