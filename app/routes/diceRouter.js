const express = require("express");

const diceRouter = express.Router();

const {postDice,getDiceHistoryByUsername,getPrizeHistoryByUsername,getVoucherHistoryByUsername } = require('../controller/diceController');

diceRouter.post('/devcamp-lucky-dice/dice',postDice);

diceRouter.get('/devcamp-lucky-dice/dice-history',getDiceHistoryByUsername);

diceRouter.get('/devcamp-lucky-dice/prize-history',getPrizeHistoryByUsername);

diceRouter.get('/devcamp-lucky-dice/voucher-history',getVoucherHistoryByUsername);


module.exports = {diceRouter} 
