const express = require("express");

const {timesandmethodMiddleware} = require("../middlewares/timeandmethodmiddleware");

const  router = express.Router();

router.use(timesandmethodMiddleware);

module.exports = {router}